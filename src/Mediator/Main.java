package Mediator;

import Mediator.Figures.Square;
import Mediator.Figures.Triangle;

public class Main {
    public static void main(String[] args) {
        Square square = new Square(3);
        Triangle triangle = new Triangle(4, 4, 7);
        square.circle();
        triangle.circle();
    }
}