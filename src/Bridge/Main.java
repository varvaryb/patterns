package Bridge;

import Bridge.Colors.Blue;
import Bridge.Colors.Color;
import Bridge.Colors.Green;
import Bridge.Colors.Red;
import Bridge.Figures.Ball;
import Bridge.Figures.Cube;
import Bridge.Figures.Pyramid;

public class Main {
    public static void main(String[] args) {
        System.out.println("Цвет Красный:");
        testFigure(new Red());
        System.out.println("Цвет Синий:");
        testFigure(new Blue());
        System.out.println("Цвет Зелёный:");
        testFigure(new Green());
    }

    public static void testFigure(Color color) {
        System.out.println("Тест с шаром");
        Ball ball = new Ball(color);
        System.out.println(ball.getColor().getColor());

        System.out.println("Тест с кубом");
        Cube cube = new Cube(color);
        System.out.println(cube.getColor().getColor());

        System.out.println("Тест с пирамидой");
        Pyramid pyramid = new Pyramid(color);
        System.out.println(pyramid.getColor().getColor());
    }
}
