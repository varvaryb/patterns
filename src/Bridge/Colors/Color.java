package Bridge.Colors;

public interface Color {
    String getColor();
}
