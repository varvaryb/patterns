package Bridge.Figures;

import Bridge.Colors.Color;

public class Ball implements Figure {
    private Color color;
    private double volume;
    private int diameter;

    public Ball(Color color) {
        this.volume = 4.19;
        this.diameter = 1;
        this.color = color;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }
}