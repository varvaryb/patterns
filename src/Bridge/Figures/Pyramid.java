package Bridge.Figures;

import Bridge.Colors.Color;

public class Pyramid implements Figure {
    private double volume;
    private int width;
    private int height;
    private int length;
    private Color color;

    public Pyramid(Color color) {
        this.volume = 0.33;
        this.width = 1;
        this.height = 1;
        this.length = 1;
        this.color = color;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }
}