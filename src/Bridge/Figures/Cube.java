package Bridge.Figures;

import Bridge.Colors.Color;

public class Cube implements Figure {
    private Color color;
    private int volume;
    private int edge;

    public Cube(Color color) {
        this.volume = 1;
        this.edge = 1;
        this.color = color;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }
}