package Bridge.Figures;

import Bridge.Colors.Color;

public interface Figure {
    Color getColor();
    void setColor(Color color);
}
