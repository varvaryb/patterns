package Adapter.ObjectAdapter;

public interface ISolid {
    double getVolume();
}
