package Adapter.ObjectAdapter;

import Adapter.Compatible.Liquid;
import Adapter.Incompatible.Solid;
import Adapter.SolidAdapter;

public class ObjectAdapter extends Liquid implements ISolid {
    public double volumeOneElem;
    public int numberOfElem;
    private SolidAdapter adapter;

    public ObjectAdapter(double volumeOneElem, int numberOfElem) {
        this.adapter = new SolidAdapter(new Solid(volumeOneElem, numberOfElem));
    }

    public ObjectAdapter(Solid solid) {
        this.adapter = new SolidAdapter(solid);
    }

    @Override
    public double getVolume() {
        return adapter.getVolume();
    }
}