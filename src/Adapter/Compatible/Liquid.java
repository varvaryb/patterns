package Adapter.Compatible;

public class Liquid {
    private double volume;

    public Liquid() {
    }

    public Liquid(double volume) {
        this.volume = volume;
    }

    public double getVolume() {
        return volume;
    }
}