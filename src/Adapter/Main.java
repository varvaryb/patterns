package Adapter;

import Adapter.Compatible.Liquid;
import Adapter.Compatible.Vessel;
import Adapter.Incompatible.Solid;
import Adapter.ObjectAdapter.ObjectAdapter;
import Adapter.ObjectAdapter.ISolid;

public class Main {
    public static void main(String[] args) {
        Vessel vessel = new Vessel(500);
        Liquid liquid = new Liquid(500);
        if (vessel.put(liquid)) {
            System.out.println("500 мл жидкости помещается в ёмкости объёмом 500");
        }

        Solid solid1 = new Solid(50, 10);
        Solid solid2 = new Solid(50, 20);
        SolidAdapter solidAdapter1 = new SolidAdapter(solid1);
        SolidAdapter solidAdapter2 = new SolidAdapter(solid2);
        if (vessel.put(solidAdapter1)) {
            System.out.println("10 шт по объёму 50 помещаются в ёмкости объёмом 500");
        }
        if (!vessel.put(solidAdapter2)) {
            System.out.println("30 шт по объёму 50 не помещаются в ёмкости объёмом 500");
        }

        ISolid solidOA1 = new ObjectAdapter(solid2);
        ISolid solidOA2 = new ObjectAdapter(25, 20);
        if (!vessel.put(solidOA1)) {
            System.out.println("ObjectAdapter: 30 шт по объёму 50 не помещаются в ёмкости объёмом 500");
        }
        if (vessel.put(solidOA2)) {
            System.out.println("ObjectAdapter: 20 шт по объёму 25 помещаются в ёмкости объёмом 500");
        }
    }
}
