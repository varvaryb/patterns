package Decorator.Jackets;

public class OffSeasonJacket extends Jacket {
    private boolean warmLining;

    public OffSeasonJacket(IJacket door, boolean hood, String material, boolean warmLining) {
        super(door, hood, material);
        this.warmLining = warmLining;
    }
}