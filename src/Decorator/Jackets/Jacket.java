package Decorator.Jackets;

public class Jacket implements IJacket {
    private int size;
    private String color;
    private boolean hood;
    private String material;

    public Jacket(int size, String color) {
        this.size = size;
        this.color = color;
    }

    public Jacket(IJacket door, boolean hood, String material) {
        this(door.getSize(), door.getColor());
        this.hood = hood;
        this.material = material;
    }

    public int getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    public boolean isHood() {
        return hood;
    }

    public String getMaterial() {
        return material;
    }
}