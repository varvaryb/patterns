package Decorator.Jackets;

public interface IJacket {
    int getSize();

    String getColor();
}
