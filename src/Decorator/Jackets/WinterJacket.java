package Decorator.Jackets;

public class WinterJacket extends Jacket {
    private String colorOfFur;
    private String typeOfFur;
    private boolean fur; // мех

    public WinterJacket(IJacket door, boolean hood, String material, boolean fur) {
        super(door, hood, material);
        this.fur = fur;
    }

    public void colorAndTypeOfFur(String colorOfFur, String typeOfFur) {
        if (fur) {
            this.colorOfFur = colorOfFur;
            this.typeOfFur = typeOfFur;
            System.out.println("Цвет и вид меховой вставки: " + colorOfFur + ", " + typeOfFur);
        }
    }
}