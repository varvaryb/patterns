package Decorator;

import Decorator.Jackets.Jacket;
import Decorator.Jackets.OffSeasonJacket;
import Decorator.Jackets.WinterJacket;

public class Main {
    public static void main(String[] args) {
        Jacket jacket = new Jacket(46, "чёрный");
        System.out.println("Создание зимней куртки без меховой вставки");
        WinterJacket winterJacket1 = new WinterJacket(jacket, true, "джинса", false);
        winterJacket1.colorAndTypeOfFur("белый", "песец");
        System.out.println("Создание зимней куртки с меховой вставкой");
        WinterJacket winterJacket2 = new WinterJacket(jacket, false, "кожа", true);
        winterJacket2.colorAndTypeOfFur("чёрный", "лиса");
        System.out.println("Создание межсезонной куртки без тёплого подклада");
        OffSeasonJacket offSeasonJacket1 = new OffSeasonJacket(jacket, false, "джинса", false);
        System.out.println("Создание межсезонной куртки с тёплым подкладом");
        OffSeasonJacket entranceDoor1 = new OffSeasonJacket(jacket, true, "хлопок", true);
    }
}
