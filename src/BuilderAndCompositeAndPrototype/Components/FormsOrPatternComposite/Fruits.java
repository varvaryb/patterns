package BuilderAndCompositeAndPrototype.Components.FormsOrPatternComposite;

import java.util.List;

public class Fruits extends Form {
    private String format = "2D";
    private int weight = 70;

    public enum Fruit {
        APPLE("Яблоко"), GARNET("Гранат"), GRAPES("Виноград");

        private String name;

        Fruit(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Fruit{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    public Fruits(List<String> details, Fruit name) {
        super(details, name.getName());
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public int getWeight() {
        return weight;
    }
}
