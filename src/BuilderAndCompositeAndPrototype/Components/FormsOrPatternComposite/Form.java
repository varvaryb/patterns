package BuilderAndCompositeAndPrototype.Components.FormsOrPatternComposite;

import java.util.List;

public class Form implements IForm {
    public List<String> details;
    public String name;

    public Form(List<String> details, String name) {
        this.details = details;
        this.name = name;
    }

    @Override
    public List<String> getDetails() {
        return details;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public int getWeight() {
        return 0;
    }

    @Override
    public String getFormat() {
        return null;
    }
}
