package BuilderAndCompositeAndPrototype.Components.FormsOrPatternComposite;

import java.util.List;

public class Figures extends Form {
    private String format = "3D";
    private int weight = 100;

    public enum Figure {
        PYRAMID("Пирамида"), BALL("Шар"), CUBE("Куб");

        private String name;

        Figure(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Figure{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    public Figures(List<String> details, Figure name) {
        super(details, name.getName());
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public String getFormat() {
        return format;
    }
}
