package BuilderAndCompositeAndPrototype.Components.FormsOrPatternComposite;

import java.util.List;

public class Flowers extends Form {
    private String format = "3D";
    private int weight = 150;

    public enum Flower {
        ROSE("Роза"), TULIP("Тюльпан"), LILY("Лилия");

        private String name;

        Flower(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Flower{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    public Flowers(List<String> details, Flower name) {
        super(details, name.getName());
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public String getFormat() {
        return format;
    }
}
