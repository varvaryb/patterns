package BuilderAndCompositeAndPrototype.Components.FormsOrPatternComposite;

import java.util.List;

public interface IForm {
    List<String> getDetails();

    String getName();

    int getWeight();

    String getFormat();
}
