package BuilderAndCompositeAndPrototype.Components;

// essential oils - эфирные масла
public enum Oil {
    ESSENTIAL, AVOCADO, GRAPE_SEED, COCOA, COCONUT
}
