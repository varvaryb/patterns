package BuilderAndCompositeAndPrototype.Components;

// отдушка или основной компонент
public enum Flavor {
    GARNET, CHOCOLATE, COCONUT, MINT, JASMINE, GRAPES, PEACH
}
