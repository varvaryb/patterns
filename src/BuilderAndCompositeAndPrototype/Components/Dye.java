package BuilderAndCompositeAndPrototype.Components;

public enum Dye {
    PINK, RED, YELLOW, BLUE, GREEN, BLACK, WHITE
}
