package BuilderAndCompositeAndPrototype.Components;

// база: твёрдые (сухие) или жидкие духи
public enum Basis {
    SOLID, LIQUID
}
