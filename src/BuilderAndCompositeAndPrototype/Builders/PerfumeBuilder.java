package BuilderAndCompositeAndPrototype.Builders;

import BuilderAndCompositeAndPrototype.Components.*;
import BuilderAndCompositeAndPrototype.Components.FormsOrPatternComposite.IForm;
import BuilderAndCompositeAndPrototype.PerfumeOrPatternPrototype.LiquidPerfume;
import BuilderAndCompositeAndPrototype.PerfumeOrPatternPrototype.SolidPerfume;

import java.util.List;

public class PerfumeBuilder implements Builder {
    private Basis basis;
    private Flavor flavor;
    private Dye basisDye;
    private DyeDetails detailsDye;
    private List<Oil> oil;
    private IForm form;
    private int weight;

    @Override
    public void setBasis(Basis basis) {
        this.basis = basis;
    }

    @Override
    public void setPerfume(Flavor flavor) {
        this.flavor = flavor;
    }

    @Override
    public void setBasicDye(Dye dye) {
        this.basisDye = dye;
    }

    @Override
    public void setDyeDetails(DyeDetails dye) {
        this.detailsDye = dye;
    }

    @Override
    public void setOil(List<Oil> oil) {
        this.oil = oil;
    }

    @Override
    public void setForm(IForm form) {
        this.form = form;
    }

    @Override
    public void setWeight(int weight) {
        this.weight = weight;
    }

    public LiquidPerfume getResultLiquid() {
        System.out.println("Созданы жидкие духи");
        return new LiquidPerfume(flavor, basisDye, weight, oil);
    }

    public SolidPerfume getResultSolid() {
        System.out.println("Созданы твердые духи");
        return new SolidPerfume(flavor, basisDye, weight, detailsDye, form);
    }
}