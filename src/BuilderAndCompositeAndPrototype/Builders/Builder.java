package BuilderAndCompositeAndPrototype.Builders;

import BuilderAndCompositeAndPrototype.Components.*;
import BuilderAndCompositeAndPrototype.Components.FormsOrPatternComposite.IForm;

import java.util.List;

public interface Builder {
    void setBasis(Basis basis);

    void setPerfume(Flavor flavor);

    void setBasicDye(Dye dye);

    void setDyeDetails(DyeDetails details);

    void setOil(List<Oil> oil);

    void setForm(IForm form);

    void setWeight(int weight);
}