package BuilderAndCompositeAndPrototype.PerfumeOrPatternPrototype;

import BuilderAndCompositeAndPrototype.Components.Basis;
import BuilderAndCompositeAndPrototype.Components.Dye;
import BuilderAndCompositeAndPrototype.Components.Flavor;
import BuilderAndCompositeAndPrototype.Components.Oil;

import java.util.List;
import java.util.Objects;

public class LiquidPerfume extends Perfume {
    private Basis basis;
    private List<Oil> oil;

    public LiquidPerfume(Flavor flavor, Dye basesDye, int weight, List<Oil> oil) {
        super(flavor, basesDye, weight);
        this.basis = Basis.LIQUID;
        this.oil = oil;
    }

    public LiquidPerfume() {
    }

    public LiquidPerfume(LiquidPerfume target) {
        super(target);
        if (target != null) {
            this.basis = Basis.LIQUID;
            this.oil = target.oil;
        }
    }

    @Override
    public Perfume clone() {
        System.out.println("Создана копия");
        return new LiquidPerfume(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LiquidPerfume)) return false;
        if (!super.equals(o)) return false;
        LiquidPerfume that = (LiquidPerfume) o;
        return Objects.equals(oil, that.oil);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), oil);
    }
}