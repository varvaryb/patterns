package BuilderAndCompositeAndPrototype.PerfumeOrPatternPrototype;

import BuilderAndCompositeAndPrototype.Components.Basis;
import BuilderAndCompositeAndPrototype.Components.Dye;
import BuilderAndCompositeAndPrototype.Components.DyeDetails;
import BuilderAndCompositeAndPrototype.Components.FormsOrPatternComposite.IForm;
import BuilderAndCompositeAndPrototype.Components.Flavor;

import java.util.Objects;

public class SolidPerfume extends Perfume {
    private Basis basis;
    private DyeDetails detailsDye;
    private IForm form;

    public SolidPerfume() {
    }

    public SolidPerfume(Flavor flavor, Dye basesDye, int weight, DyeDetails detailsDye, IForm form) {
        super(flavor, basesDye, weight);
        this.detailsDye = detailsDye;
        this.form = form;
        this.basis = Basis.SOLID;
    }

    public SolidPerfume(SolidPerfume target) {
        super(target);
        if (target != null) {
            this.basis = Basis.SOLID;
            this.detailsDye = target.detailsDye;
            this.form = target.form;
        }

    }

    @Override
    public Perfume clone() {
        System.out.println("Создана копия");
        return new SolidPerfume(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SolidPerfume)) return false;
        if (!super.equals(o)) return false;
        SolidPerfume solidSoap = (SolidPerfume) o;
        return Objects.equals(detailsDye, solidSoap.detailsDye) &&
                Objects.equals(form, solidSoap.form);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), detailsDye, form);
    }
}