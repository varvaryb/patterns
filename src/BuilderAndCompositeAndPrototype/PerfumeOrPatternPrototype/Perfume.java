package BuilderAndCompositeAndPrototype.PerfumeOrPatternPrototype;

import BuilderAndCompositeAndPrototype.Components.Dye;
import BuilderAndCompositeAndPrototype.Components.Flavor;

import java.util.Objects;

public abstract class Perfume {
    private Flavor flavor;
    private Dye basesDye;
    private int weight;

    public Perfume() {
    }

    public Perfume(Flavor flavor, Dye basesDye, int weight) {
        this.flavor = flavor;
        this.basesDye = basesDye;
        this.weight = weight;
    }

    public Perfume(Perfume target) {
        if (target != null) {
            this.basesDye = target.basesDye;
            this.flavor = target.flavor;
            this.weight = target.weight;
        }
    }

    public abstract Perfume clone();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Perfume)) return false;
        Perfume perfume = (Perfume) o;
        return weight == perfume.weight &&
                flavor == perfume.flavor &&
                basesDye == perfume.basesDye;
    }

    @Override
    public int hashCode() {
        return Objects.hash(flavor, basesDye, weight);
    }
}
