package BuilderAndCompositeAndPrototype;

import BuilderAndCompositeAndPrototype.Builders.Builder;
import BuilderAndCompositeAndPrototype.Components.*;
import BuilderAndCompositeAndPrototype.Components.FormsOrPatternComposite.Flowers;
import BuilderAndCompositeAndPrototype.Components.FormsOrPatternComposite.Fruits;
import BuilderAndCompositeAndPrototype.Components.FormsOrPatternComposite.IForm;

import java.util.ArrayList;
import java.util.List;

public class Director {
    public void constructLiquidPerfumeFlowers(Builder builder) {
        List<Oil> oils = new ArrayList<>();
        oils.add(Oil.ESSENTIAL);
        oils.add(Oil.COCOA);

        builder.setBasis(Basis.LIQUID);
        builder.setBasicDye(Dye.RED);
        builder.setOil(oils);
        builder.setWeight(150);
        builder.setPerfume(Flavor.CHOCOLATE);
        System.out.println("Вы создали жидкие духи");
    }

    public void constructSolidPerfumeRedApple(Builder builder) {
        List<String> details = new ArrayList<>();
        details.add("Оболочка");
        details.add("Веточка");
        details.add("Листочки");
        List<Dye> dye = new ArrayList<>();
        dye.add(Dye.RED);
        dye.add(Dye.BLACK);
        dye.add(Dye.GREEN);
        DyeDetails dyeDetails = new DyeDetails(details, dye);
        IForm form = new Fruits(details, Fruits.Fruit.APPLE);

        builder.setBasis(Basis.SOLID);
        builder.setBasicDye(Dye.RED);
        builder.setPerfume(Flavor.MINT);
        builder.setWeight(form.getWeight());
        builder.setForm(form);
        builder.setDyeDetails(dyeDetails);
        System.out.println("Вы создали твёрдые духи в виде красного яблока");
    }

    public void constructSolidPerfumeLily(Builder builder) {
        List<String> details = new ArrayList<>();
        details.add("Лепестки");
        details.add("Тычинки");
        details.add("Стебель");
        List<Dye> dye = new ArrayList<>();
        dye.add(Dye.WHITE);
        dye.add(Dye.BLACK);
        dye.add(Dye.GREEN);
        DyeDetails dyeDetails = new DyeDetails(details, dye);
        IForm form = new Flowers(details, Flowers.Flower.LILY);

        builder.setBasis(Basis.SOLID);
        builder.setBasicDye(Dye.WHITE);
        builder.setPerfume(Flavor.JASMINE);
        builder.setWeight(form.getWeight());
        builder.setForm(form);
        builder.setDyeDetails(dyeDetails);
        System.out.println("Вы создали твёрдые духи в виде лилии");
    }
}