package BuilderAndCompositeAndPrototype;

import BuilderAndCompositeAndPrototype.Builders.PerfumeBuilder;
import BuilderAndCompositeAndPrototype.PerfumeOrPatternPrototype.LiquidPerfume;
import BuilderAndCompositeAndPrototype.PerfumeOrPatternPrototype.SolidPerfume;

public class Main {
    public static void main(String[] args) {
        Director director = new Director();
        PerfumeBuilder builder = new PerfumeBuilder();

        director.constructLiquidPerfumeFlowers(builder);
        LiquidPerfume liquidPerfume = builder.getResultLiquid();
        liquidPerfume.clone();

        director.constructSolidPerfumeRedApple(builder);
        SolidPerfume solidPerfume1 = builder.getResultSolid();
        solidPerfume1.clone();

        director.constructSolidPerfumeLily(builder);
        SolidPerfume solidPerfume2 = builder.getResultSolid();
    }
}
