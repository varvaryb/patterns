package FactoryMethod;

import FactoryMethod.Sort.CountingSort;
import FactoryMethod.Sort.ISort;
import FactoryMethod.Sort.RadixSort;
import FactoryMethod.Sort.SelectionSort;

/**
 * 1. Требуется отсортировать целочисленную числовую последовательность
 * (например, сгенерированную случайным образом).
 * 2. Метод сортировки может быть выбран любой из указанных в вашем списке.
 * 3. Три класса сортировки реализуют интерфейс ISort для 3 указанных методов.
 * 4. Клиент класса, реализующего фабричный метод, обращается к классу по поводу
 * сортировки своей последовательности одним из методов.
 */

public class Main {
    public static void main(String[] args) {
        ISort sort1 = new RadixSort(0, 5, 3, 6);
        sort1.sort();
        print(sort1.getArray());
        ISort sort2 = new CountingSort(4, 3, 7, 9, 0);
        sort2.sort();
        print(sort2.getArray());
        ISort sort3 = new SelectionSort(4, 5, 7, 2, 9, 0);
        sort3.sort();
        print(sort3.getArray());
    }

    public static void print(int[] array) {
        for (int elem : array) {
            System.out.print(elem + " ");
        }
        System.out.print("\n");
    }
}
