package FactoryMethod.Sort;

// Сортировка выбором
public class SelectionSort implements ISort {
    private int[] array;

    public SelectionSort(Integer... array1) {
        array = new int[array1.length];
        for (int i = 0; i < array1.length; i++) {
            array[i] = array1[i];
        }
    }

    @Override
    public int[] getArray() {
        return array;
    }

    @Override
    public void sort() {
        for (int left = 0; left < array.length; left++) {
            int minInd = left;
            for (int i = left; i < array.length; i++) {
                if (array[i] < array[minInd]) {
                    minInd = i;
                }
            }
            int tmp = array[left];
            array[left] = array[minInd];
            array[minInd] = tmp;
        }
    }
}
