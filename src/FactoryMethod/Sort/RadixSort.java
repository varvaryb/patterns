package FactoryMethod.Sort;

import java.util.*;

// Поразрядная сортировка
public class RadixSort implements ISort {
    private int[] array;

    public RadixSort(Integer... array1) {
        array = new int[array1.length];
        for (int i = 0; i < array1.length; i++) {
            array[i] = array1[i];
        }
    }

    @Override
    public int[] getArray() {
        return array;
    }

    @Override
    public void sort() {
        int mx = array[0];
        for (int i = 1; i < array.length; i++)
            if (array[i] > mx)
                mx = array[i];
        int m = mx;

        for (int exp = 1; m / exp > 0; exp *= 10) {
            int output[] = new int[array.length];
            int i;
            int count[] = new int[10];
            Arrays.fill(count, 0);

            for (i = 0; i < array.length; i++)
                count[(array[i] / exp) % 10]++;

            for (i = 1; i < 10; i++)
                count[i] += count[i - 1];

            for (i = array.length - 1; i >= 0; i--) {
                output[count[(array[i] / exp) % 10] - 1] = array[i];
                count[(array[i] / exp) % 10]--;
            }

            for (i = 0; i < array.length; i++)
                array[i] = output[i];
        }
    }
}
