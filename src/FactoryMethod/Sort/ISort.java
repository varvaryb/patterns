package FactoryMethod.Sort;

public interface ISort {
    void sort();

    int[] getArray();
}
