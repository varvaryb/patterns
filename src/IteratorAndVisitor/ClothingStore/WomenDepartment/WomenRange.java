package IteratorAndVisitor.ClothingStore.WomenDepartment;

public enum WomenRange {
    TSHIRT("футболка"),
    PANTS("брюки"),
    DRESS("платье"),
    SKIRT("юбка"),
    BLOUSE("блузка");

    private String name;

    WomenRange(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
