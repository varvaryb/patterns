package IteratorAndVisitor.ClothingStore.WomenDepartment;

public class Price {
    public static int womenPrice(String str) {
        if (str.equalsIgnoreCase(WomenRange.BLOUSE.getName())) {
            return 1000;
        }
        if (str.equalsIgnoreCase(WomenRange.DRESS.getName())) {
            return 2500;
        }
        if (str.equalsIgnoreCase(WomenRange.SKIRT.getName())) {
            return 900;
        }
        if (str.equalsIgnoreCase(WomenRange.PANTS.getName())) {
            return 1500;
        }
        if (str.equalsIgnoreCase(WomenRange.TSHIRT.getName())) {
            return 470;
        }
        return 0;
    }
}
