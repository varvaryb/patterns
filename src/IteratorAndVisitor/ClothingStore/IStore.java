package IteratorAndVisitor.ClothingStore;

import java.util.List;

public interface IStore {
    int cost(List<String> order);

    void makeOrder(List<String> order);

    String getName();
}
