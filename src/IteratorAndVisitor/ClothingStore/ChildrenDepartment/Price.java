package IteratorAndVisitor.ClothingStore.ChildrenDepartment;

public class Price {
    public static int kidsPrice(String str) {
        if (str.equalsIgnoreCase(KidsRange.BODYSUIT.getName())) {
            return 500;
        }
        if (str.equalsIgnoreCase(KidsRange.SWEATER.getName())) {
            return 700;
        }
        if (str.equalsIgnoreCase(KidsRange.DRESS.getName())) {
            return 1100;
        }
        if (str.equalsIgnoreCase(KidsRange.SOCKS.getName())) {
            return 200;
        }
        if (str.equalsIgnoreCase(KidsRange.PANTS.getName())) {
            return 600;
        }
        if (str.equalsIgnoreCase(KidsRange.TSHIRT.getName())) {
            return 320;
        }
        return 0;
    }
}
