package IteratorAndVisitor.ClothingStore.ChildrenDepartment;

public enum KidsRange {
    BODYSUIT("боди"),
    TSHIRT("футболка"),
    SWEATER("кофта"),
    PANTS("штаны"),
    DRESS("платье"),
    SOCKS("носки");

    private String name;

    KidsRange(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
