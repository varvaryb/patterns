package IteratorAndVisitor.Visitor;

import java.util.List;

public interface IVisitor {
    void visit(List<String> str);
}
