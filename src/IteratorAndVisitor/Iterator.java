package IteratorAndVisitor;

import IteratorAndVisitor.ClothingStore.ChildrenDepartment.ChildrenDepartment;
import IteratorAndVisitor.ClothingStore.IStore;
import IteratorAndVisitor.ClothingStore.MenDepartment.MenDepartment;
import IteratorAndVisitor.ClothingStore.WomenDepartment.WomenDepartment;

public class Iterator {
    private int index = -1;

    public boolean hasNext() {
        if (index == -1 || index == 0 || index == 1) {
            return true;
        }
        return false;
    }

    public IStore next() {
        if (index == -1) {
            index = 0;
            return new WomenDepartment();
        }
        if (index == 0) {
            index = 1;
            return new MenDepartment();
        }
        if (index == 1) {
            index = 2;
            return new ChildrenDepartment();
        }
        return null;
    }
}
