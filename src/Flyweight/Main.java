package Flyweight;

import Flyweight.Cubes.CubePoint;

public class Main {
    public static void main(String[] args) {
        CubePoint cubPoint = new CubePoint();
        String colors1 = "Красный Жёлтый Оранжевый Розовый Фиолетовый Малиновый";
        cubPoint.pointCub("One",colors1);
        String colors2 = "Зеленый Синий Коричневый Чёрный Серый Белый";
        cubPoint.pointCub("Two",colors2);
        cubPoint.point();
    }
}
