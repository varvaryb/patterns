package Flyweight.Cubes;

import java.util.ArrayList;
import java.util.List;

public class CubePoint {
    private List<Cube> cubes = new ArrayList<>();

    public void pointCub(String name, String colors) {
        CubeType cubeType = CubeFactory.getCubeType(name, colors);
        Cube cube = new Cube(cubeType);
        cubes.add(cube);
    }

    public void point() {
        for (Cube cube : cubes) {
            System.out.println(cube.paint());
        }
    }
}
