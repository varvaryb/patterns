package Flyweight.Cubes;

public class Cube {
    private String edges;
    private CubeType type;

    public Cube(CubeType type) {
        edges = "Передняя" + " " + "Задняя" + " " + "Верхняя" + " " + "Нижняя" + " " +
                "Правая" + " " + "Левая";
        this.type = type;
    }

    public String paint() {
        return type.paint(edges);
    }
}
