package Flyweight.Cubes;

import java.util.HashMap;
import java.util.Map;

public class CubeFactory {
    private static Map<String, CubeType> cubeType = new HashMap<>();

    public static CubeType getCubeType(String name, String colors) {
        CubeType result = cubeType.get(name);
        if (result == null) {
            result = new CubeType(colors);
            cubeType.put(name, result);
        }
        return result;
    }
}
