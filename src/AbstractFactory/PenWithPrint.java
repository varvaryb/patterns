package AbstractFactory;

public class PenWithPrint implements IPen {
    private String type;
    private String inkColor;
    private double size;

    public PenWithPrint(String type, String inkColor, double size) {
        setType(type);
        setInkColor(inkColor);
        setSize(size);
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getInkColor() {
        return inkColor;
    }

    @Override
    public void setInkColor(String inkColor) {
        this.inkColor = inkColor;
    }

    @Override
    public double getSize() {
        return size;
    }

    @Override
    public void setSize(double size) {
        this.size = size;
    }

    @Override
    public void print() {
        System.out.println("Создана ручка с принтом '" + getType() + "', чернилами цвета '"
                + getInkColor() + "' и толщиной стержня " + getSize());
    }
}
