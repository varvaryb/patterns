package AbstractFactory;

import AbstractFactory.Factories.FactoryWithPrint;
import AbstractFactory.Factories.FactoryWithoutPrint;
import AbstractFactory.Factories.IFactory;

public class AbstractFactory {
    private static Shop configure(String type, String penType, String inkColor, double penSize, int sheets,
                                  String diarySize, String diaryType) {
        Shop shop;
        IFactory factory;
        if (type.equalsIgnoreCase(("withPrint"))) {
            factory = new FactoryWithPrint();
            shop = new Shop(factory, penType, inkColor, penSize, sheets, diarySize, diaryType);
        } else {
            factory = new FactoryWithoutPrint();
            shop = new Shop(factory, penType, inkColor, penSize, sheets, diarySize, diaryType);
        }
        return shop;
    }

    public static void main(String[] args) {
        Shop shop = configure("withPrint", "flowers", "red", 0.7, 60,
                "middle", "flowers");
        shop.print();
        Shop shop1 = configure("withoutPrint", "black", "blue", 0.5, 96,
                "large", "black");
        shop1.print();
    }
}
