package AbstractFactory;

import AbstractFactory.Factories.IFactory;

public class Shop {
    private IDiary diary;
    private IPen pen;

    public Shop(IFactory factory, String penType, String inkColor, double penSize, int sheets,
                String diarySize, String diaryType) {
        diary = factory.createDiary(sheets, diarySize, diaryType);
        pen = factory.createPen(penType, inkColor, penSize);
    }

    public void print() {
        diary.print();
        pen.print();
    }
}
