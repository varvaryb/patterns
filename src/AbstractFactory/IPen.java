package AbstractFactory;

public interface IPen {
    String getType();

    void setType(String type);

    String getInkColor();

    void setInkColor(String inkColor);

    double getSize();

    void setSize(double size);

    void print();
}
