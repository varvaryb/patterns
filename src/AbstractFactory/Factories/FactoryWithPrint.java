package AbstractFactory.Factories;

import AbstractFactory.DiaryWithPrint;
import AbstractFactory.IDiary;
import AbstractFactory.IPen;
import AbstractFactory.PenWithPrint;

public class FactoryWithPrint implements IFactory {
    @Override
    public IPen createPen(String type, String inkColor, double size) {
        return new PenWithPrint(type, inkColor, size);
    }

    @Override
    public IDiary createDiary(int sheets, String size, String type) {
        return new DiaryWithPrint(sheets, size, type);
    }
}
