package AbstractFactory.Factories;

import AbstractFactory.IDiary;
import AbstractFactory.IPen;

public interface IFactory {
    IPen createPen(String type, String inkColor, double size);

    IDiary createDiary(int sheets, String size, String type);
}
