package AbstractFactory.Factories;

import AbstractFactory.DiaryWithoutPrint;
import AbstractFactory.IDiary;
import AbstractFactory.IPen;
import AbstractFactory.PenWithoutPrint;

public class FactoryWithoutPrint implements IFactory {
    @Override
    public IPen createPen(String type, String inkColor, double size) {
        return new PenWithoutPrint(type, inkColor, size);
    }

    @Override
    public IDiary createDiary(int sheets, String size, String type) {
        return new DiaryWithoutPrint(sheets, size, type);
    }
}