package Strategy;

import Strategy.Strategy.RadixSort;
import Strategy.Strategy.CountingSort;
import Strategy.Strategy.SelectionSort;

public class Main {
    public static void main(String[] args) {
        int[] array1 = new int[]{4, 5, 7, 2, 9, 0, 1, 0, 3, 5};
        int[] array2 = new int[]{1, 4, 7, 2, 5, 8, 3, 0, 6, 9};
        int[] array3 = new int[]{6, 1, 4, 0, 5, 3, 6, 8, 7, 2};

        Sorter sorter = new Sorter(new RadixSort());
        sorter.sortAndPrint(array1);
        sorter.setSort(new CountingSort());
        sorter.sortAndPrint(array2);
        sorter.setSort(new SelectionSort());
        sorter.sortAndPrint(array3);
    }
}