package Strategy.Strategy;

public interface SortStrategy {
    void sorting(int[] array);
}
