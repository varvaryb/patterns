package Observer;

public class Main {
    public static void main(String[] args) {
        Reader HarryPotter1 = new Reader("Гарри Поттер");
        Reader HarryPotter2 = new Reader("Гарри Поттер", "Гарри Поттер и узник Азкабана (1999)");
        Reader HarryPotter3 = new Reader("Гарри Поттер", 7);

        Reader TheBartimaeusTrilogy1 = new Reader("Трилогия Бартимеуса");
        Reader TheBartimaeusTrilogy2 = new Reader("Трилогия Бартимеуса", 3);

        Reader Chasodei1 = new Reader("Часодеи", "Часовая башня (2012)");
        Reader Chasodei2 = new Reader("Часодеи", 4);

        Observer observer = new Observer();
        observer.setNextBook(HarryPotter1);
        observer.setNextBook(HarryPotter2);
        observer.setNextBook(HarryPotter3);
        System.out.print("\n");
        observer.setNextBook(TheBartimaeusTrilogy1);
        observer.setNextBook(TheBartimaeusTrilogy2);
        System.out.print("\n");
        observer.setNextBook(Chasodei1);
        observer.setNextBook(Chasodei2);
    }
}
