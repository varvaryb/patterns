package Observer;

import java.util.ArrayList;
import java.util.List;

public class Observer {
    private List<String> list;

    public void setNextBook(Reader reader) {
        if (reader.getSeries().equalsIgnoreCase("Гарри Поттер")) {
            nextBooKSeries(reader, listHP());
        }
        if (reader.getSeries().equalsIgnoreCase("Трилогия Бартимеуса")) {
            nextBooKSeries(reader, listTB());
        }
        if (reader.getSeries().equalsIgnoreCase("Часодеи")) {
            nextBooKSeries(reader, listC());
        }
    }

    public static void nextBooKSeries(Reader reader, List<String> list) {
        if (reader.getNumBook() == 0) {
            if (null == reader.getBook()) {
                System.out.println(list.get(0));
            } else {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).equalsIgnoreCase(reader.getBook())) {
                        if (i + 1 < list.size()) {
                            System.out.println(list.get(i + 1));
                        } else {
                            System.out.println("Книги этой серии закончились");
                        }
                    }
                }
            }
        } else {
            if (reader.getNumBook() < list.size()) {
                System.out.println(list.get(reader.getNumBook()));
            } else {
                System.out.println("Книги этой серии закончились");
            }
        }
    }

    public static List<String> listHP() {
        List<String> list = new ArrayList<>();
        list.add("Гарри Поттер и философский камень (1997)");
        list.add("Гарри Поттер и Тайная комната (1998)");
        list.add("Гарри Поттер и узник Азкабана (1999)");
        list.add("Гарри Поттер и Кубок огня (2000)");
        list.add("Гарри Поттер и Орден Феникса (2003)");
        list.add("Гарри Поттер и Принц-полукровка (2005)");
        list.add("Гарри Поттер и Дары Смерти (2007)");
        return list;
    }

    public static List<String> listTB() {
        List<String> list = new ArrayList<>();
        list.add("Амулет Самарканда (2003)");
        list.add("Глаз голема (2004)");
        list.add("Врата Птолемея (2005)");
        list.add("Кольцо Соломона (2010, приквел)");
        return list;
    }

    public static List<String> listC() {
        List<String> list = new ArrayList<>();
        list.add("Часовой ключ (2011)");
        list.add("Часовое сердце (2011)");
        list.add("Часовая башня (2012)");
        list.add("Часовое имя (2013)");
        list.add("Часограмма (2014)");
        list.add("Часовая битва (2015)");
        return list;
    }
}
