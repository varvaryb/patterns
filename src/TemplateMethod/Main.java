package TemplateMethod;

import TemplateMethod.Arrays.AbArray;
import TemplateMethod.Arrays.DoubleArray;
import TemplateMethod.Arrays.IntArray;

public class Main {
    public static void main(String[] args) {
        AbArray array1 = new IntArray(5);
        array1.printArray();
        AbArray array2 = new DoubleArray(5);
        array2.printArray();
    }
}
