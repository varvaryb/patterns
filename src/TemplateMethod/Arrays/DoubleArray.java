package TemplateMethod.Arrays;

public class DoubleArray extends AbArray {
    private double[] array;

    public DoubleArray(int length) {
        createAndFill(length);
    }

    @Override
    void createAndFill(int length) {
        array = new double[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = -10.0 + Math.random() * 20.0;
        }
    }

    @Override
    public void printArray() {
        System.out.println("Исходный массив");
        for (double elem : array) {
            System.out.format("%.2f  ", elem);
        }
        System.out.println();
        array = sort(array);
        System.out.println("Отсортированный массив");
        for (double elem1 : array) {
            System.out.format("%.2f  ", elem1);
        }
        System.out.println();
        System.out.println();
    }
}