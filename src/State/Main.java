package State;

public class Main {
    public static void main(String[] args) {
        Anime anime1 = new Anime("Дороро (2019)", 24);
        Anime anime2 = new Anime("Магическая битва", 24, 12);
        Anime anime3 = new Anime("Стальной алхимик: Братство", 64, 64);

        System.out.println(anime1.getName());
        anime1.getAnimeState().watch(anime1);
        System.out.println();
        System.out.println(anime2.getName());
        anime2.getAnimeState().watch(anime2);
        System.out.println();
        System.out.println(anime3.getName());
        anime3.getAnimeState().watch(anime3);
    }
}
