package State.State;

import State.Anime;

public class AnimeStart implements IAnimeState {
    @Override
    public String getStateName() {
        return StatusName.START.getName();
    }

    @Override
    public void watch(Anime anime) {
        System.out.println("Количество серий: " + anime.getNumberOfSeries());
        System.out.println("Приятного просмотра");
    }
}