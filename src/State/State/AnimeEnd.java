package State.State;

import State.Anime;

public class AnimeEnd implements IAnimeState {
    @Override
    public String getStateName() {
        return StatusName.END.getName();
    }

    @Override
    public void watch(Anime anime) {
        System.out.println("Аниме просмотрено");
    }
}