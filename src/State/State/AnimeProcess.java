package State.State;

import State.Anime;

public class AnimeProcess implements IAnimeState {
    @Override
    public String getStateName() {
        return StatusName.PROCESS.getName();
    }

    @Override
    public void watch(Anime anime) {
        int res = anime.getNumberOfSeries() - anime.getNumOfViewedSeries();
        System.out.println("Осталось посмотреть: " + res + " серий");
    }
}