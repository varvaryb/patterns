package State.State;

public enum StatusName {
    START("Начало просмотра"),
    PROCESS("В процессе"),
    END("Просмотрено");

    private String name;

    StatusName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
