package State.State;

import State.Anime;

public interface IAnimeState {
    String getStateName();

    void watch(Anime anime);
}
