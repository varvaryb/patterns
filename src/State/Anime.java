package State;

import State.State.AnimeEnd;
import State.State.AnimeProcess;
import State.State.AnimeStart;
import State.State.IAnimeState;

public class Anime {
    private String name;
    private int numberOfSeries;
    private int numOfViewedSeries;
    private IAnimeState animeState;

    public Anime(String name, int numberOfSeries) {
        this.name = name;
        this.numberOfSeries = numberOfSeries;
        this.animeState = new AnimeStart();
    }

    public Anime(String name, int numberOfSeries, int numOfViewedSeries) {
        this.name = name;
        this.numberOfSeries = numberOfSeries;
        this.numOfViewedSeries = numOfViewedSeries;
        if (numOfViewedSeries == numberOfSeries) {
            this.animeState = new AnimeEnd();
        } else {
            this.animeState = new AnimeProcess();
        }
    }

    public String getName() {
        return name;
    }

    public int getNumberOfSeries() {
        return numberOfSeries;
    }

    public int getNumOfViewedSeries() {
        return numOfViewedSeries;
    }

    public IAnimeState getAnimeState() {
        return animeState;
    }
}
