package Memento;

public class Main {
    public static void main(String[] args) {
        Square square = new Square(4);
        Editor editor = new Editor();
        editor.increaseEdge(square, 3);
        editor.decreaseEdge(square, 2);
        editor.changeEdge(square, 7);
        editor.print();
    }
}
