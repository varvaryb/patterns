package Memento;

public class Editor {
    private Memento memento = new Memento(new Square(1));

    public void increaseEdge(Square square, int num) {
        memento.addInHistory(square);
        square.increaseEdgeBy(num);
    }

    public void decreaseEdge(Square square, int num) {
        memento.addInHistory(square);
        square.decreaseEdgeBy(num);
    }

    public void changeEdge(Square square, int edge) {
        memento.addInHistory(square);
        square.changeEdge(edge);
    }

    public void print() {
        memento.getHistory().print();
    }
}
