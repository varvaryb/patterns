package Memento;

public class Square {
    private int edge;

    public Square(int edge) {
        this.edge = edge;
    }

    public void increaseEdgeBy(int num) {
        edge = edge + num;
    }

    public void decreaseEdgeBy(int num) {
        if (edge > num) {
            edge = edge - num;
        } else {
            System.out.println("Число больше ребра");
        }
    }

    public void changeEdge(int edge1) {
        if (edge1 > 0) {
            edge = edge1;
        } else {
            System.out.println("Введено неправильное число");
        }
    }

    public void print() {
        System.out.print("Квадрат: ");
        System.out.print(edge + "  " + "\n");
    }

    public int getEdge() {
        return edge;
    }
}
