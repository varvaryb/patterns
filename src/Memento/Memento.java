package Memento;

public class Memento {
    private static History history = new History();
    private Square square;

    public Memento(Square square) {
        this.square = new Square(square.getEdge());
    }

    public void addInHistory(Square square) {
        Memento memento = new Memento(square);
        history.add(memento);
    }

    public void print() {
        square.print();
    }

    public History getHistory() {
        return history;
    }
}
