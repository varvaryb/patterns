package Proxy;

import Proxy.Squares.ISquare;
import Proxy.Squares.Square;

public class SquareProxy implements ISquare {
    private ISquare iSquare;
    private Square result = null;

    public SquareProxy(Square square) {
        this.iSquare = square;
    }

    @Override
    public Square moveTo(int x, int y) {
        if (result == null) {
            result = iSquare.moveTo(x, y);
        }
        return result;
    }

    @Override
    public Square stretch(int xRatio, int yRatio) {
        if (result == null) {
            result = iSquare.stretch(xRatio, yRatio);
        }
        return result;
    }

    @Override
    public void clearProxy() {
        result = null;
    }
}
