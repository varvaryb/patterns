package Proxy;

import Proxy.Squares.ISquare;
import Proxy.Squares.Square;

public class PrintSquare {
    private ISquare square;

    public PrintSquare(SquareProxy proxy) {
        this.square = proxy;
    }

    public void printSquare(int x, int y, int xRatio, int yRatio) {
        Square result1 = square.stretch(xRatio, yRatio);
        System.out.println("Растянутый квадрат: " + "центр (" + result1.getCenter().getX() + ", " +
                result1.getCenter().getY() + "), ширина " + result1.getWidth() + ", длина " + result1.getLength());
        square.clearProxy();
        Square result2 = square.moveTo(x, y);
        System.out.println("Передвинутый квадрат: " + "центр (" + result2.getCenter().getX() + ", " +
                result2.getCenter().getY() + "), ширина " + result2.getWidth() + ", длина " + result2.getLength());
    }
}