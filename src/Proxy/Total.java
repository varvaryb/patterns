package Proxy;

import Proxy.Squares.Point;
import Proxy.Squares.Square;

public class Total {
    public static void main(String[] args) {
        Square square = new Square(new Point(), 1, 1);
        PrintSquare printSquare = new PrintSquare(new SquareProxy(square));
        printSquare.printSquare(1, 1, 5, 6);
        System.out.println("Первоначальнаый квадрат: " + "центр (" + square.getCenter().getX() + ", " +
                square.getCenter().getY() + "), ширина " + square.getWidth() + ", длина " + square.getLength());
    }
}
