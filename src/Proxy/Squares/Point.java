package Proxy.Squares;

public class Point {
    private int x, y;

    public Point(int x, int y) {
        setX(x);
        setY(y);
    }

    public Point() {
        this(0, 0);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Point moveTo(int newX, int newY) {
        return new Point(newX, newY);
    }
}
