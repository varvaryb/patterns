package Proxy.Squares;

public class Square implements ISquare {
    private Point center;
    private int width;
    private int length;

    public Square(Point center, int width, int length) {
        this.center = center;
        this.width = width;
        this.length = length;
    }

    public Square() {
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    public Point getCenter() {
        return center;
    }

    @Override
    public Square moveTo(int x, int y) {
        return new Square(center.moveTo(x, y), width, length);
    }

    @Override
    public Square stretch(int xRatio, int yRatio) {
        return new Square(center, width * xRatio, length * yRatio);
    }

    @Override
    public void clearProxy() {
    }
}
