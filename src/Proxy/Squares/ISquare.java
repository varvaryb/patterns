package Proxy.Squares;

public interface ISquare {
    Square moveTo(int x, int y);

    Square stretch(int xRatio, int yRatio);

    void clearProxy();
}
