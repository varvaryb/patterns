package ChainOfCommandAndCommand;

import ChainOfCommandAndCommand.OrderElems.OrderPrice;
import ChainOfCommandAndCommand.OrderElems.Sequencing;
import ChainOfCommandAndCommand.OrderElems.StockCheck;

import java.util.ArrayList;
import java.util.List;

public class Main {
    private static Order order;

    public static void start() {
        order = new Order();

        Sequencing sequencing = new StockCheck(order).linkWith(new OrderPrice(order));

        order.setSequencing(sequencing);
    }

    public static List<String> list1() {
        List<String> booking1 = new ArrayList<>();
        booking1.add("Сухой корм для кошек");
        booking1.add("Консервы для кошек");
        booking1.add("Наполнитель для кошачего туалета");
        booking1.add("Сухой корм для собак");
        booking1.add("Консервы для собак");
        booking1.add("Игрушка мячик для собаки");
        booking1.add("Аквариум");
        booking1.add("Консервы для кошек");
        booking1.add("Наполнитель для кошачего туалета");
        return booking1;
    }

    public static List<String> list2() {
        List<String> booking1 = new ArrayList<>();
        booking1.add("Корм для рыбок");
        booking1.add("Украшения для аквариума");
        booking1.add("Маленькие улитки");
        booking1.add("Лежанка для кошки");
        booking1.add("Лежанка для собаки");
        booking1.add("Кошачий ошейник");
        booking1.add("Собачий поводок");
        booking1.add("Намордник");
        return booking1;
    }

    public static void main(String[] args) {
        start();
        System.out.println("Сумма первого заказа:");
        order.registration(list1());
        System.out.println("Сумма второго заказа:");
        order.registration(list2());
    }
}