package ChainOfCommandAndCommand.Stock;

public enum ForFish {
    VITAMINS("Витамины для рыбок"),
    FOOD("Корм для рыбок"),
    AQUARIUM("Аквариум"),
    HOUSE("Домик"),
    DECORATION("Украшения для аквариума"),
    ALGAE("Водоросли"),
    LITTLE_SNAIL("Маленькие улитки");

    private String name;

    ForFish(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static boolean check(String elem) {
        return elem.equalsIgnoreCase(LITTLE_SNAIL.name) || elem.equalsIgnoreCase(ALGAE.name) ||
                elem.equalsIgnoreCase(DECORATION.name) || elem.equalsIgnoreCase(HOUSE.name) ||
                elem.equalsIgnoreCase(AQUARIUM.name) || elem.equalsIgnoreCase(FOOD.name) ||
                elem.equalsIgnoreCase(VITAMINS.name);
    }
}
