package ChainOfCommandAndCommand.Stock;

public enum ForDogs {
    DRY_FOOD("Сухой корм для собак"),
    WET_FOOD("Консервы для собак"),
    VITAMINS("Витамины для собак"),
    MUZZLE("Намордник"),
    LEAD("Собачий поводок"),
    BEDROOM("Лежанка для собаки"),
    BOWL("Собачья миска для корма"),
    BALL("Игрушка мячик для собаки");

    private String name;

    ForDogs(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static boolean check(String elem) {
        return elem.equalsIgnoreCase(BALL.name) || elem.equalsIgnoreCase(BOWL.name) ||
                elem.equalsIgnoreCase(BEDROOM.name) || elem.equalsIgnoreCase(LEAD.name) ||
                elem.equalsIgnoreCase(MUZZLE.name) || elem.equalsIgnoreCase(VITAMINS.name) ||
                elem.equalsIgnoreCase(WET_FOOD.name) || elem.equalsIgnoreCase(DRY_FOOD.name);
    }
}
