package ChainOfCommandAndCommand.Stock;

public enum ForCats {
    DRY_FOOD("Сухой корм для кошек"),
    WET_FOOD("Консервы для кошек"),
    VITAMINS("Витамины для кошек"),
    BOWL("Кошачья миска для корма"),
    BEDROOM("Лежанка для кошки"),
    TOILET_FILLER("Наполнитель для кошачего туалета"),
    CLUTCH("Когтеточка"),
    COLLAR("Кошачий ошейник");

    private String name;

    ForCats(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static boolean check(String elem) {
        return elem.equalsIgnoreCase(CLUTCH.name) || elem.equalsIgnoreCase(TOILET_FILLER.name) ||
                elem.equalsIgnoreCase(BEDROOM.name) || elem.equalsIgnoreCase(BOWL.name) ||
                elem.equalsIgnoreCase(VITAMINS.name) || elem.equalsIgnoreCase(WET_FOOD.name) ||
                elem.equalsIgnoreCase(DRY_FOOD.name) || elem.equalsIgnoreCase(COLLAR.name);
    }
}
