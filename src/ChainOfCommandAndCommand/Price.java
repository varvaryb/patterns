package ChainOfCommandAndCommand;

import ChainOfCommandAndCommand.Stock.ForFish;
import ChainOfCommandAndCommand.Stock.ForCats;
import ChainOfCommandAndCommand.Stock.ForDogs;

public class Price {
    public static int price(String str) {
        if (str.equalsIgnoreCase(ForFish.VITAMINS.getName())) {
            return 150;
        }
        if (str.equalsIgnoreCase(ForFish.FOOD.getName())) {
            return 300;
        }
        if (str.equalsIgnoreCase(ForFish.AQUARIUM.getName())) {
            return 2500;
        }
        if (str.equalsIgnoreCase(ForFish.HOUSE.getName())) {
            return 700;
        }
        if (str.equalsIgnoreCase(ForFish.DECORATION.getName())) {
            return 500;
        }
        if (str.equalsIgnoreCase(ForFish.ALGAE.getName())) {
            return 200;
        }
        if (str.equalsIgnoreCase(ForFish.LITTLE_SNAIL.getName())) {
            return 300;
        }

        if (str.equalsIgnoreCase(ForCats.DRY_FOOD.getName())) {
            return 350;
        }
        if (str.equalsIgnoreCase(ForCats.WET_FOOD.getName())) {
            return 150;
        }
        if (str.equalsIgnoreCase(ForCats.VITAMINS.getName())) {
            return 200;
        }
        if (str.equalsIgnoreCase(ForCats.BOWL.getName())) {
            return 180;
        }
        if (str.equalsIgnoreCase(ForCats.BEDROOM.getName())) {
            return 1500;
        }
        if (str.equalsIgnoreCase(ForCats.TOILET_FILLER.getName())) {
            return 900;
        }
        if (str.equalsIgnoreCase(ForCats.CLUTCH.getName())) {
            return 2000;
        }
        if (str.equalsIgnoreCase(ForCats.COLLAR.getName())) {
            return 500;
        }

        if (str.equalsIgnoreCase(ForDogs.DRY_FOOD.getName())) {
            return 500;
        }
        if (str.equalsIgnoreCase(ForDogs.WET_FOOD.getName())) {
            return 250;
        }
        if (str.equalsIgnoreCase(ForDogs.VITAMINS.getName())) {
            return 350;
        }
        if (str.equalsIgnoreCase(ForDogs.MUZZLE.getName())) {
            return 500;
        }
        if (str.equalsIgnoreCase(ForDogs.LEAD.getName())) {
            return 600;
        }
        if (str.equalsIgnoreCase(ForDogs.BEDROOM.getName())) {
            return 3000;
        }
        if (str.equalsIgnoreCase(ForDogs.BOWL.getName())) {
            return 300;
        }
        if (str.equalsIgnoreCase(ForDogs.BALL.getName())) {
            return 250;
        }
        return 0;
    }
}
