package FacadeAndSingleton;

import FacadeAndSingleton.Search.Songs;

public final class SearchFacade {
    private static SearchFacade instance;

    private SearchFacade() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static SearchFacade getInstance() {
        if (instance == null) {
            instance = new SearchFacade();
        }
        return instance;
    }

    public Songs getListSingle(String type, String genre) {
        return new Songs(type, genre);
    }

    public static void print(Songs songs) {
        songs.print();
    }
}
