package FacadeAndSingleton.Search;

import java.util.ArrayList;
import java.util.List;

public class Album {
    public static List<String> getList(String name) {
        List<String> list = new ArrayList<>();
        if (name.equalsIgnoreCase(Genre.ROCK.getName())) {
            list.add("Nighwish 'Decades')");
            list.add("Within Temptation 'The Silent Force'");
            list.add("Three Days Grace 'One-X'");
            list.add("Asking Alexandria 'Asking Alexandria')");
            list.add("Skillet 'Comatose'");
            list.add("Linkin Park 'Meteora'");
            list.add("STARSET 'Transmissions'");
            list.add("30 Seconds to Mars 'This is War'");
        }

        if (name.equalsIgnoreCase(Genre.KPOP.getName())) {
            list.add("BTS 'BE'");
            list.add("SEVENTEEN 'An Ode'");
            list.add("Agust D 'D-2'");
            list.add("ATEEZ 'ZERO: FEVER Part.1'");
            list.add("Stray Kids 'IN LIFE'");
            list.add("ASTRO 'All Light'");
        }

        if (name.equalsIgnoreCase(Genre.POP.getName())) {
            list.add("Alec Benjamin 'These Two Windows'");
            list.add("Lauv '~how i'm feeling~'");
            list.add("Halsey 'Manic'");
            list.add("5 Seconds of Summer 'Youngblood'");
            list.add("Melanie Martinez 'Cry Baby'");
        }
        return list;
    }
}
