package FacadeAndSingleton.Search;

import java.util.ArrayList;
import java.util.List;

public class Songs {
    private List<String> list;

    public Songs(String type, String genre) {
        this.list = getRecommendation(type, genre);
    }

    public List<String> getRecommendation(String type, String genre) {
        List<String> result = new ArrayList<>();
        if (Type.ALBUM.getName().equalsIgnoreCase(type)) {
            result = Album.getList(genre);
        }
        if (Type.SINGLE.getName().equalsIgnoreCase(type)) {
            result = Single.getList(genre);
        }
        return result;
    }

    public void print() {
        for (String elem : list) {
            System.out.println(elem);
        }
    }
}
