package FacadeAndSingleton.Search;

import java.util.ArrayList;
import java.util.List;

public class Single {
    public static List<String> getList(String name) {
        List<String> list = new ArrayList<>();
        if (name.equalsIgnoreCase(Genre.ROCK.getName())) {
            list.add("Linkin Park - In the End");
            list.add("Nightwish - Amaranth");
            list.add("Within Temptation - What Have You Done");
            list.add("СЛОТ - Мёртвые звёзды");
            list.add("Bring Me The Horizon - Can You Feel My Heart");
            list.add("Lunatica - Who You Are");
            list.add("Brothers of Metal - Yggdrasil");
        }
        if (name.equalsIgnoreCase(Genre.KPOP.getName())) {
            list.add("BTS - Life Goes On");
            list.add("SEVENTEEN - Fearless)");
            list.add("(G)I-DLE - Oh my God");
            list.add("Stray Kids - MIROH");
            list.add("ASTRO - Knock");
            list.add("ITZY - WANNABE");
            list.add("ATEEZ - HALA HALA");
        }
        if (name.equalsIgnoreCase(Genre.POP.getName())) {
            list.add("Ariana Grande - Positions");
            list.add("Lauv - Who");
            list.add("Sam Smith - Dancing With A Stranger");
            list.add("Matisyahu - Live Like A Warrior");
            list.add("Melanie Martinez - Play Date");
            list.add("Halsey - Nightmare");
            list.add("Nea - Some Say");
        }
        return list;
    }
}
