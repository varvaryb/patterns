package FacadeAndSingleton.Search;

public enum Genre {
    ROCK("Рок"),
    KPOP("K-pop"),
    POP("Поп-музыка");

    private String name;

    Genre(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "name='" + name + '\'' +
                '}';
    }
}
