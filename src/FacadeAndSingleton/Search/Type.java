package FacadeAndSingleton.Search;

public enum Type {
    SINGLE("Сингл"),
    ALBUM("Альбом");

    private String name;

    Type(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Type{" +
                "name='" + name + '\'' +
                '}';
    }
}
