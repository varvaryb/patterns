package FacadeAndSingleton;

import java.util.Scanner;

import static FacadeAndSingleton.SearchFacade.print;

public class Main {
    public static void main(String[] args) {
        SearchFacade searchFacade = SearchFacade.getInstance();
        Scanner in = new Scanner(System.in);
        System.out.println("Введите жанр: Рок, K-pop, Поп-музыка");
        String genre = in.nextLine();
        System.out.println("Введите тип: Сингл, Альбом");
        String type = in.nextLine();
        print(searchFacade.getListSingle(type, genre));
    }
}
